# Read Me 
Projeto realizado para apresentação a SIBS

- Inicialmente será necessario efetuar a criação dobanco de dados com o arquivo scriptBD localizado em src/main/resources/bd/scriptBD.sql
- Após a criação do BD, podemos configurar o envio de email ou executar sem o mesmo. Caso queira Executar o envio de email verificar a configuração na sessão abaixo chamada configurações extra.
- Para chamada dos endpoints, pode-se utilizar o postman.json criado em src/main/resources/postman/postman.json e importar as chamadas GET, POST, PUT e DELETE.


# Start sistema 
Para realizar o start do sistema, basta executar a classe DemoApplication.java er eefetuar as devidas chamadas via rest.

# Resolução do exercicios
Configuraçõpes de chamada rest
Para realizar a chamada da ativida proposta, chamar no Pacote Order o endpoint createOrder com as seguintes configurações:
- endpoint POST http://localhost:8080/sibs/order
- Selecionar Body - dentro do campo selecionar "raw" e formato "json"
- inserir o request: 
{
  "item": {
    "id": 2,
    "name": "Teclado"
  },
  "quantity": 2,
  "user": {
    "id": 1,
    "name": "João Silva",
    "email": "joao.silva@email.com"
  }
}

Efetuar a chamada do endpoint.

# Configurações extra
Se você deseja configurar o e-mail no arquivo application.properties, as propriedades básicas a serem configuradas são:

spring.mail.host=smtp.example.com
spring.mail.port=587
spring.mail.username=your_email@example.com
spring.mail.password=your_email_password
spring.mail.properties.mail.smtp.auth=true
spring.mail.properties.mail.smtp.starttls.enable=true

Substitua smtp.example.com pelo host SMTP do seu provedor de e-mail, your_email@example.com pelo seu endereço de e-mail e your_email_password pela senha do seu e-mail.

Certifique-se de que as propriedades estejam corretas de acordo com o provedor de e-mail que você está usando. Dependendo do provedor de e-mail e das configurações específicas de segurança, você pode precisar ajustar outras propriedades ou usar diferentes portas, como 465 para SSL/TLS.

Após essa configuração, descomentar o método sendSimpleMessage dentro da classe EmailServiceImpl.java e também descomentar na classe OrderServiceImpl as linhas de injeção de dependencia 46 e 47 e as linhas 93 à 99 do método createOrder da mesma classe.

