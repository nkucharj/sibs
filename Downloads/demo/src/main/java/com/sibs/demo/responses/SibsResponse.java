package com.sibs.demo.responses;

import java.io.Serializable;

public class SibsResponse<T> implements Serializable {

	private static final long serialVersionUID = 4226015719967149493L;

	private String status;
	private String code;
	private String message;
	private T data;

	public SibsResponse() {
			super();
		}

	public SibsResponse(String status, String code, String message) {
			this.status = status;
			this.code = code;
			this.message = message;
		}

	public SibsResponse(String status, String code, String message, T data) {
			this.status = status;
			this.code = code;
			this.message = message;
			this.data = data;
		}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
