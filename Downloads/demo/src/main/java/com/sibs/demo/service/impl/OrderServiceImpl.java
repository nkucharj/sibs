package com.sibs.demo.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sibs.demo.exceptions.InsufficientStockException;
import com.sibs.demo.exceptions.InternalServerErrorException;
import com.sibs.demo.exceptions.NotFoundException;
import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.ItemRest;
import com.sibs.demo.json.OrderRest;
import com.sibs.demo.json.StockMovementRest;
import com.sibs.demo.model.Item;
import com.sibs.demo.model.Order;
import com.sibs.demo.model.User;
import com.sibs.demo.repository.ItemRepository;
import com.sibs.demo.repository.OrderRepository;
import com.sibs.demo.repository.UserRepository;
import com.sibs.demo.service.OrderService;
import com.sibs.demo.service.StockMovementService;
import com.sibs.demo.service.StockService;
import com.sibs.demo.utils.constants.ExceptionConstants;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private StockService stockService;

	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private UserRepository userRepository;
	
//	@Autowired
//	private EmailService emailService;
	
    @Autowired
    private StockMovementService stockMovementService;

	private ModelMapper modelMapper = new ModelMapper();

	private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Override
	public OrderRest createOrder(OrderRest orderRest) throws SibsException {
		
	    Order order = new Order();
	    order.setQuantityCompleted(0);

	    // Buscar a entidade Item do banco de dados
	    Item item = itemRepository.findById(orderRest.getItem().getId())
	        .orElseThrow(() -> new NotFoundException("Item not found"));

	    // Buscar a entidade User do banco de dados
	    User user = userRepository.findById(orderRest.getUser().getId())
	        .orElseThrow(() -> new NotFoundException("User not found"));

	    order.setItem(item);
	    order.setQuantity(orderRest.getQuantity());
	    order.setUser(user);
	    order.setCreationDate(LocalDateTime.now());
		
		ItemRest itemRest = new ItemRest();
		itemRest.setId(item.getId());
		itemRest.setName(item.getName());
		try {
			// Verificar a disponibilidade de itens no estoque

			int availableStock = stockService.getAvailableStock(item);
			if (availableStock < order.getQuantity()) {
			    throw new InsufficientStockException("Not enough items in stock");
			}
			order = orderRepository.save(order);
			LOGGER.info("Order created: {}", order.getId());
			// Atualizar o estoque e atribuir movimentos de estoque a pedidos incompletos
			stockService.processStockAndIncompleteOrders(itemRest, order.getQuantity());
			// Processar a conclusão da ordem
	        processOrderCompletion(order);
	        LOGGER.info("Order completed: {}", order.getId());
	        
	        /***   // Enviar e-mail de notificação ao usuário que criou o pedido
	        String userEmail = order.getUser().getEmail();
	        String subject = "Seu pedido foi concluído";
	        String messageText = "Olá, seu pedido #" + order.getId() + " foi concluído com sucesso!";
	        
	        //Necessário alterar servidor SMTP no arquivo application.properties
	        emailService.sendSimpleMessage(userEmail, subject, messageText);***/
			
		} catch (final Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(order, OrderRest.class);
	}

	@Override
	public List<OrderRest> getOrders() throws SibsException {
		return orderRepository.findAll().stream().map(order -> modelMapper.map(order, OrderRest.class))
				.collect(Collectors.toList());
	}

	@Override
	public OrderRest updateOrder(Long id, OrderRest orderRest) throws SibsException {
		Order order = new Order();
		try {
			order = orderRepository.getReferenceById(id);
			order.setItem(orderRest.getItem());
			order.setQuantity(orderRest.getQuantity());
			order.setUser(orderRest.getUser());
			orderRepository.save(order);
		} catch (Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(order, OrderRest.class);
	}

	@Override
	public OrderRest deleteOrder(Long id) throws SibsException {
		Order order = new Order();
		try {
			order = orderRepository.getReferenceById(id);
			orderRepository.delete(order);
		} catch (Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(order, OrderRest.class);
	}

	   private void processOrderCompletion(Order order) throws SibsException {
//	        order.setStatus("COMPLETED");
//	        orderRepository.save(order);

	        // Criar um novo movimento de estoque para a ordem concluída
		   StockMovementRest stockMovement = new StockMovementRest();
	        stockMovement.setItem(order.getItem());
	        stockMovement.setQuantity(order.getQuantity());
	        stockMovement.setCreationDate(LocalDateTime.now());

	        // Salvar o movimento de estoque e atualizar o estoque
	        try {
				stockMovementService.createStockMovement(stockMovement);
			} catch (SibsException e) {
				LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
				throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
			}
	    }
}
