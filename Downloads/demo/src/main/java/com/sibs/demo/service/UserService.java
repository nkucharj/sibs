package com.sibs.demo.service;

import java.util.List;

import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.UserRest;

public interface UserService {

	List<UserRest> getUsers() throws SibsException;
	
	UserRest createUser(UserRest userRest) throws SibsException;
	
	UserRest updateUser(Long id, UserRest userRest) throws SibsException;

	UserRest deleteUser(Long id) throws SibsException;
}
