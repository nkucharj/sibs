package com.sibs.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sibs.demo.model.StockMovement;

public interface StockMovementRepository extends JpaRepository<StockMovement, Long>  {

}
