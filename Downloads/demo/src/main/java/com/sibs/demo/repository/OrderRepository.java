package com.sibs.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sibs.demo.model.Item;
import com.sibs.demo.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

	List<Order> findIncompleteOrdersByItem(Item item);

}
