package com.sibs.demo.exceptions;

public class InsufficientStockException extends RuntimeException {

	private static final long serialVersionUID = -1298762590849098179L;

	public InsufficientStockException(String message) {
		super(message);
	}

}
