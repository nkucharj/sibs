package com.sibs.demo.exceptions;

import java.util.ArrayList;
import java.util.List;

import com.sibs.demo.dto.ErrorDto;

public class SibsException extends Exception {

	private static final long serialVersionUID = 5170271431243417519L;

	private final int code;

	private final List<ErrorDto> errorList = new ArrayList<>();

	public SibsException(final int code, final String message) {
		super(message);
		this.code = code;
	}

	public SibsException(final int code, final String message, final List<ErrorDto> errorList) {
		super(message);
		this.code = code;
		this.errorList.addAll(errorList);
	}

	public int getCode() {
		return this.code;
	}

	public List<ErrorDto> getErrorList() {
		return errorList;
	}
}
