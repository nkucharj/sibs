package com.sibs.demo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.StockMovementRest;
import com.sibs.demo.responses.SibsResponse;

public interface StockMovementController {

	SibsResponse<List<StockMovementRest>> getStockMovement() throws SibsException;

	SibsResponse<StockMovementRest> createStockMovement(@RequestBody final StockMovementRest stockMovementRest) throws SibsException;

	SibsResponse<StockMovementRest> updateStockMovement(@PathVariable Long id, @RequestBody final StockMovementRest stockMovementRest) throws SibsException;

	SibsResponse<StockMovementRest> deleteStockMovement(@PathVariable Long id) throws SibsException;
}
