package com.sibs.demo.utils.constants;

public class ExceptionConstants {

	public static final String ERROR = "ERROR";

	public static final String INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR - An internal server error has ocurred";
	
	public static final String BAD_REQUEST = "Não há itens suficientes disponíveis para criar o pedido.";

	private ExceptionConstants() {
		throw new IllegalStateException("Utility Class");
	}

}
