package com.sibs.demo.exceptions;

import java.util.Arrays;

import org.springframework.http.HttpStatus;

import com.sibs.demo.dto.ErrorDto;

public class InternalServerErrorException extends SibsException {

	private static final long serialVersionUID = -5999825895948943610L;

	public InternalServerErrorException(final String message) {
		super(HttpStatus.INTERNAL_SERVER_ERROR.value(), message);
	}

	public InternalServerErrorException(final String message, final ErrorDto data) {
		super(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, Arrays.asList(data));
	}
}
