package com.sibs.demo.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sibs.demo.exceptions.InternalServerErrorException;
import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.ItemRest;
import com.sibs.demo.json.OrderRest;
import com.sibs.demo.json.StockRest;
import com.sibs.demo.model.Item;
import com.sibs.demo.model.Order;
import com.sibs.demo.model.Stock;
import com.sibs.demo.repository.OrderRepository;
import com.sibs.demo.repository.StockMovementRepository;
import com.sibs.demo.repository.StockRepository;
import com.sibs.demo.service.StockService;
import com.sibs.demo.utils.constants.ExceptionConstants;

@Service
public class StockServiceImpl implements StockService {

	@Autowired
	private StockRepository stockRepository;
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private StockMovementRepository stockMovementRepository;

	private ModelMapper modelMapper = new ModelMapper();

	private static final Logger LOGGER = LoggerFactory.getLogger(StockServiceImpl.class);

	@Override
	public StockRest createStock(StockRest stockRest) throws SibsException {
		Stock stock = new Stock();
		stock.setItem(stockRest.getItem());
		stock.setQuantity(stockRest.getQuantity());

		try {
			stock = stockRepository.save(stock);
		} catch (final Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(stock, StockRest.class);
	}

	@Override
	public List<StockRest> getStock() throws SibsException {
		return stockRepository.findAll().stream().map(stock -> modelMapper.map(stock, StockRest.class))
				.collect(Collectors.toList());
	}

	@Override
	public StockRest updateStock(Long id, StockRest stockRest) throws SibsException {
		Stock stock = new Stock();
		try {
			stock = stockRepository.getReferenceById(id);
			stock.setItem(stockRest.getItem());
			stock.setQuantity(stockRest.getQuantity());
			stockRepository.save(stock);
		} catch (Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(stock, StockRest.class);
	}

	@Override
	public StockRest deleteStock(Long id) throws SibsException {
		Stock stock = new Stock();
		try {
			stock = stockRepository.getReferenceById(id);
			stockRepository.delete(stock);
		} catch (Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(stock, StockRest.class);
	}

	/*********
	 * ******* ******* Lógicas movimentação de stock ******* *******
	 *******/

	@Override
	public void processStockAndIncompleteOrders(ItemRest itemRest, int quantity) {
		// Atualize o estoque
		updateStockProc(itemRest, quantity);

		// Atribuir movimentos de estoque a pedidos incompletos
		assignStockMovementsToIncompleteOrders(itemRest, quantity);
	}

	private void updateStockProc(ItemRest itemRest, int quantity) {
		Item item = new Item();
		item.setId(itemRest.getId());
		item.setName(itemRest.getName());
		Optional<Stock> stockOptional = stockRepository.findByItem(item);

		if (stockOptional.isPresent()) {
			Stock stockRest = stockOptional.get();
			stockRest.setQuantity(stockRest.getQuantity() + quantity);

			Stock stock = new Stock();
			stock.setItem(stockRest.getItem());
			stock.setQuantity(stockRest.getQuantity());

			stockRepository.save(stock);
		} else {
			Stock stock = new Stock();
			stock.setItem(item);
			stock.setQuantity(quantity);
			stockRepository.save(stock);
		}
	}

	private void assignStockMovementsToIncompleteOrders(ItemRest itemRest, int quantity) {
		Item item = new Item();
		item.setId(itemRest.getId());
		item.setName(itemRest.getName());
		List<Order> incompleteOrders = orderRepository.findIncompleteOrdersByItem(item);

		for (Order order : incompleteOrders) {
			int remainingQuantity = order.getQuantity() - order.getQuantityCompleted();

			if (quantity >= remainingQuantity) {
				// Se a quantidade disponível é suficiente para completar o pedido
				order.setQuantityCompleted(order.getQuantity());
				quantity -= remainingQuantity;
			} else {
				// Se a quantidade disponível é insuficiente para completar o pedido
				order.setQuantityCompleted(order.getQuantityCompleted() + quantity);
				quantity = 0;
			}

			orderRepository.save(order);

			if (quantity == 0) {
				break;
			}
		}
	}

	@Override
	public int getAvailableStock(Item item) {

		Optional<Stock> stockOptional = stockRepository.findByItem(item);
		return stockOptional.map(stock -> stock.getQuantity()).orElse(0);
	}

}
