package com.sibs.demo.service;

public interface EmailService {

	void sendSimpleMessage(String userEmail, String subject, String messageText);

}
