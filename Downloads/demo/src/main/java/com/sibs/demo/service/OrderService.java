package com.sibs.demo.service;

import java.util.List;

import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.OrderRest;

public interface OrderService {

	List<OrderRest> getOrders() throws SibsException;
	
	OrderRest createOrder(OrderRest orderRest) throws SibsException;
	
	OrderRest updateOrder(Long id, OrderRest orderRest) throws SibsException;

	OrderRest deleteOrder(Long id) throws SibsException;
}
