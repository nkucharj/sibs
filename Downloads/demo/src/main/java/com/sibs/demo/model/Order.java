package com.sibs.demo.model;

import java.time.LocalDateTime;
import java.util.Objects;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Entity
@Table(schema = "sibs", name = "order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_id_seq")
    @SequenceGenerator(schema = "sibs", name = "order_id_seq", sequenceName = "order_id_seq", allocationSize = 1)
    private Long id;

    private LocalDateTime creationDate;

    @OneToOne
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    private Item item;

    private Integer quantity;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;
    
    @Transient
    private Integer quantityCompleted;

	public Order() {
		super();
	}

	public Integer getQuantityCompleted() {
		return quantityCompleted;
	}

	public void setQuantityCompleted(Integer quantityCompleted) {
		this.quantityCompleted = quantityCompleted;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public int hashCode() {
		return Objects.hash(creationDate, id, item, quantity, user);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		return Objects.equals(creationDate, other.creationDate) && Objects.equals(id, other.id)
				&& Objects.equals(item, other.item) && quantity == other.quantity && Objects.equals(user, other.user);
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", creationDate=" + creationDate + ", item=" + item + ", quantity=" + quantity
				+ ", user=" + user + "]";
	}
}
