package com.sibs.demo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.ItemRest;
import com.sibs.demo.responses.SibsResponse;

public interface ItemController {

	SibsResponse<List<ItemRest>> getItems() throws SibsException;

	SibsResponse<ItemRest> createItem(@RequestBody final ItemRest itemRest) throws SibsException;

	SibsResponse<ItemRest> updateItem(@PathVariable Long id, @RequestBody final ItemRest itemRest) throws SibsException;

	SibsResponse<ItemRest> deleteItem(@PathVariable Long id) throws SibsException;

}
