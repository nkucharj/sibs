package com.sibs.demo.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sibs.demo.controller.OrderController;
import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.OrderRest;
import com.sibs.demo.responses.SibsResponse;
import com.sibs.demo.service.impl.OrderServiceImpl;
import com.sibs.demo.utils.constants.CommonConstants;
import com.sibs.demo.utils.constants.RestConstants;

@RestController
@RequestMapping(RestConstants.RESOURCE_ORDER)
public class OrderControllerImp implements OrderController {

    @Autowired
    private OrderServiceImpl orderService;

	@Override
	@ResponseStatus(HttpStatus.OK)
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<List<OrderRest>> getOrders() throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				orderService.getOrders());
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<OrderRest> createOrder(@RequestBody final OrderRest orderRest) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				orderService.createOrder(orderRest));
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PutMapping(value = RestConstants.RESOURCE_ID + RestConstants.ORDER, produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<OrderRest> updateOrder(@PathVariable Long id, @RequestBody OrderRest orderRest) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK, orderService.updateOrder(id, orderRest));
	}
	
	@Override
	@ResponseStatus(HttpStatus.OK)
	@DeleteMapping(value = RestConstants.RESOURCE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<OrderRest> deleteOrder(@PathVariable Long id) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK, orderService.deleteOrder(id));
	}
}
