package com.sibs.demo.service;

import java.util.List;

import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.ItemRest;

public interface ItemService {

	List<ItemRest> getItems() throws SibsException;
	
	ItemRest createItem(ItemRest itemRest) throws SibsException;
	
	ItemRest updateItem(Long id, ItemRest itemRest) throws SibsException;

	ItemRest deleteItem(Long id) throws SibsException;
}
