package com.sibs.demo.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sibs.demo.controller.UserController;
import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.UserRest;
import com.sibs.demo.responses.SibsResponse;
import com.sibs.demo.service.impl.UserServiceImpl;
import com.sibs.demo.utils.constants.CommonConstants;
import com.sibs.demo.utils.constants.RestConstants;

@RestController
@RequestMapping(RestConstants.RESOURCE_USER)
public class UserControllerImp implements UserController {

    @Autowired
    private UserServiceImpl userService;

	@Override
	@ResponseStatus(HttpStatus.OK)
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<List<UserRest>> getUsers() throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				userService.getUsers());
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<UserRest> createUser(@RequestBody final UserRest userRest) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				userService.createUser(userRest));
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PutMapping(value = RestConstants.RESOURCE_ID + RestConstants.USER, produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<UserRest> updateUser(@PathVariable Long id, @RequestBody UserRest userRest) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK, userService.updateUser(id, userRest));
	}
	
	@Override
	@ResponseStatus(HttpStatus.OK)
	@DeleteMapping(value = RestConstants.RESOURCE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<UserRest> deleteUser(@PathVariable Long id) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK, userService.deleteUser(id));
	}
}
