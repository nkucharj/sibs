package com.sibs.demo.exceptions;

import java.util.Arrays;

import org.springframework.http.HttpStatus;

import com.sibs.demo.dto.ErrorDto;

public class NotFoundException extends SibsException{

	private static final long serialVersionUID = 4031982075644031399L;

	public NotFoundException(final String message) {
		super(HttpStatus.NOT_FOUND.value(), message);
	}

	public NotFoundException(final String message, final ErrorDto data) {
		super(HttpStatus.NOT_FOUND.value(), message, Arrays.asList(data));
	}
}
