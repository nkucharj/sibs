package com.sibs.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sibs.demo.model.Item;

public interface ItemRepository extends JpaRepository<Item, Long>  {

}
