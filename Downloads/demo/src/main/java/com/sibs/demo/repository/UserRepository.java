package com.sibs.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sibs.demo.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
