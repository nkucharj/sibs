package com.sibs.demo.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sibs.demo.exceptions.InternalServerErrorException;
import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.UserRest;
import com.sibs.demo.model.User;
import com.sibs.demo.repository.UserRepository;
import com.sibs.demo.service.UserService;
import com.sibs.demo.utils.constants.ExceptionConstants;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	private ModelMapper modelMapper = new ModelMapper();

	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@Override
	public UserRest createUser(UserRest userRest) throws SibsException {
		User user = new User();
		user.setName(userRest.getName());
		user.setEmail(userRest.getEmail());

		try {
			user = userRepository.save(user);
		} catch (final Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(user, UserRest.class);
	}

	@Override
	public List<UserRest> getUsers() throws SibsException {
		return userRepository.findAll().stream().map(user -> modelMapper.map(user, UserRest.class)).collect(Collectors.toList());
	}

	@Override
	public UserRest updateUser(Long id, UserRest userRest) throws SibsException {
		User user = new User();
		try {
			user = userRepository.getReferenceById(id);
			user.setName(userRest.getName());
			user.setEmail(userRest.getEmail());
			userRepository.save(user);
		} catch (Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(user, UserRest.class);
	}

	@Override
	public UserRest deleteUser(Long id) throws SibsException {
		User user = new User();
		try {
			user = userRepository.getReferenceById(id);
			userRepository.delete(user);
		} catch (Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(user, UserRest.class);
	}

}
