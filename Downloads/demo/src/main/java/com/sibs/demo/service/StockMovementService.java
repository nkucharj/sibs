package com.sibs.demo.service;

import java.util.List;

import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.StockMovementRest;

public interface StockMovementService {

	List<StockMovementRest> getStockMovement() throws SibsException;
	
	StockMovementRest createStockMovement(StockMovementRest stockMovementRest) throws SibsException;
	
	StockMovementRest updateStockMovement(Long id, StockMovementRest stockMovementRest) throws SibsException;

	StockMovementRest deleteStockMovement(Long id) throws SibsException;
}
