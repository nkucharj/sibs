package com.sibs.demo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.UserRest;
import com.sibs.demo.responses.SibsResponse;

public interface UserController {

	SibsResponse<List<UserRest>> getUsers() throws SibsException;

	SibsResponse<UserRest> createUser(@RequestBody final UserRest userRest) throws SibsException;

	SibsResponse<UserRest> updateUser(@PathVariable Long id, @RequestBody final UserRest userRest) throws SibsException;

	SibsResponse<UserRest> deleteUser(@PathVariable Long id) throws SibsException;
}
