package com.sibs.demo.service;

import java.util.List;

import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.ItemRest;
import com.sibs.demo.json.StockRest;
import com.sibs.demo.model.Item;

public interface StockService {

	List<StockRest> getStock() throws SibsException;
	
	StockRest createStock(StockRest stockRest) throws SibsException;
	
	StockRest updateStock(Long id, StockRest stockRest) throws SibsException;

	StockRest deleteStock(Long id) throws SibsException;
	
	void processStockAndIncompleteOrders(ItemRest itemRest, int quantity) throws SibsException;
	
	int getAvailableStock(Item item) throws SibsException;
}