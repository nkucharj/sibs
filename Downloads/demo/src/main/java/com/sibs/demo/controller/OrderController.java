package com.sibs.demo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.OrderRest;
import com.sibs.demo.responses.SibsResponse;

public interface OrderController {

	SibsResponse<List<OrderRest>> getOrders() throws SibsException;

	SibsResponse<OrderRest> createOrder(@RequestBody final OrderRest orderRest) throws SibsException;

	SibsResponse<OrderRest> updateOrder(@PathVariable Long id, @RequestBody final OrderRest orderRest) throws SibsException;

	SibsResponse<OrderRest> deleteOrder(@PathVariable Long id) throws SibsException;
}
