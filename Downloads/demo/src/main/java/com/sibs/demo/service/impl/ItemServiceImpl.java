package com.sibs.demo.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sibs.demo.exceptions.InternalServerErrorException;
import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.ItemRest;
import com.sibs.demo.model.Item;
import com.sibs.demo.repository.ItemRepository;
import com.sibs.demo.service.ItemService;
import com.sibs.demo.utils.constants.ExceptionConstants;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemRepository itemRepository;

	private ModelMapper modelMapper = new ModelMapper();

	private static final Logger LOGGER = LoggerFactory.getLogger(ItemServiceImpl.class);

	@Override
	public ItemRest createItem(ItemRest itemRest) throws SibsException {
		Item item = new Item();
		item.setName(itemRest.getName());

		try {
			item = itemRepository.save(item);
		} catch (final Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(item, ItemRest.class);
	}

	@Override
	public List<ItemRest> getItems() throws SibsException {
		return itemRepository.findAll().stream().map(item -> modelMapper.map(item, ItemRest.class)).collect(Collectors.toList());
	}

	@Override
	public ItemRest updateItem(Long id, ItemRest itemRest) throws SibsException {
		Item item = new Item();
		try {
			item = itemRepository.getReferenceById(id);
			item.setName(itemRest.getName());
			itemRepository.save(item);
		} catch (Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(item, ItemRest.class);
	}

	@Override
	public ItemRest deleteItem(Long id) throws SibsException {
		Item item = new Item();
		try {
			item = itemRepository.getReferenceById(id);
			itemRepository.delete(item);
		} catch (Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(item, ItemRest.class);
	}

}
