package com.sibs.demo.json;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.sibs.demo.model.Item;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class StockMovementRest implements Serializable {

	private static final long serialVersionUID = 6889662604106937510L;

	private Long id;

	private LocalDateTime creationDate;

	private Item item;

	private Integer quantity;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
}
