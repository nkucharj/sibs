package com.sibs.demo.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sibs.demo.controller.StockController;
import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.StockRest;
import com.sibs.demo.responses.SibsResponse;
import com.sibs.demo.service.impl.StockServiceImpl;
import com.sibs.demo.utils.constants.CommonConstants;
import com.sibs.demo.utils.constants.RestConstants;
@RestController
@RequestMapping(RestConstants.RESOURCE_STOCK)
public class StockControllerImp implements StockController {

	@Autowired
	private StockServiceImpl stockService;

	@Override
	@ResponseStatus(HttpStatus.OK)
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<List<StockRest>> getStock() throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				stockService.getStock());
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<StockRest> createStock(@RequestBody final StockRest stockRest) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				stockService.createStock(stockRest));
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PutMapping(value = RestConstants.RESOURCE_ID + RestConstants.ITEM, produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<StockRest> updateStock(@PathVariable Long id, @RequestBody StockRest stockRest) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK, stockService.updateStock(id, stockRest));
	}
	
	@Override
	@ResponseStatus(HttpStatus.OK)
	@DeleteMapping(value = RestConstants.RESOURCE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<StockRest> deleteStock(@PathVariable Long id) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK, stockService.deleteStock(id));
	}
}
