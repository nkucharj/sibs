package com.sibs.demo.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.StockRest;
import com.sibs.demo.responses.SibsResponse;

public interface StockController {

	SibsResponse<List<StockRest>> getStock() throws SibsException;

	SibsResponse<StockRest> createStock(@RequestBody final StockRest stockRest) throws SibsException;

	SibsResponse<StockRest> updateStock(@PathVariable Long id, @RequestBody final StockRest stockRest) throws SibsException;

	SibsResponse<StockRest> deleteStock(@PathVariable Long id) throws SibsException;
}
