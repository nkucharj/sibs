package com.sibs.demo.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sibs.demo.exceptions.InternalServerErrorException;
import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.StockMovementRest;
import com.sibs.demo.model.StockMovement;
import com.sibs.demo.repository.StockMovementRepository;
import com.sibs.demo.service.StockMovementService;
import com.sibs.demo.utils.constants.ExceptionConstants;

@Service
public class StockMovementServiceImpl implements StockMovementService{

    @Autowired
    private StockMovementRepository stockMovementRepository;

	private ModelMapper modelMapper = new ModelMapper();

	private static final Logger LOGGER = LoggerFactory.getLogger(StockMovementServiceImpl.class);

	@Override
	public StockMovementRest createStockMovement(StockMovementRest stockMovementRest) throws SibsException {
		StockMovement stockMovement = new StockMovement();
		stockMovement.setItem(stockMovementRest.getItem());
		stockMovement.setQuantity(stockMovementRest.getQuantity());
		stockMovement.setCreationDate(LocalDateTime.now());

		try {
			stockMovement = stockMovementRepository.save(stockMovement);
		} catch (final Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(stockMovement, StockMovementRest.class);
	}

	@Override
	public List<StockMovementRest> getStockMovement() throws SibsException {
		return stockMovementRepository.findAll().stream().map(stockmovement -> modelMapper.map(stockmovement, StockMovementRest.class)).collect(Collectors.toList());
	}

	@Override
	public StockMovementRest updateStockMovement(Long id, StockMovementRest stockMovementRest) throws SibsException {
		StockMovement stockMovement = new StockMovement();
		try {
			stockMovement = stockMovementRepository.getReferenceById(id);
			stockMovement.setItem(stockMovementRest.getItem());
			stockMovement.setQuantity(stockMovementRest.getQuantity());
			stockMovementRepository.save(stockMovement);
		} catch (Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(stockMovement, StockMovementRest.class);
	}

	@Override
	public StockMovementRest deleteStockMovement(Long id) throws SibsException {
		StockMovement stockMovement = new StockMovement();
		try {
			stockMovement = stockMovementRepository.getReferenceById(id);
			stockMovementRepository.delete(stockMovement);
		} catch (Exception e) {
			LOGGER.error(ExceptionConstants.INTERNAL_SERVER_ERROR, e);
			throw new InternalServerErrorException(ExceptionConstants.INTERNAL_SERVER_ERROR);
		}
		return modelMapper.map(stockMovement, StockMovementRest.class);
	}


}
