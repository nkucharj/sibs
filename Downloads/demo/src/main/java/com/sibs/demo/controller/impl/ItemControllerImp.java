package com.sibs.demo.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sibs.demo.controller.ItemController;
import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.ItemRest;
import com.sibs.demo.responses.SibsResponse;
import com.sibs.demo.service.ItemService;
import com.sibs.demo.utils.constants.CommonConstants;
import com.sibs.demo.utils.constants.RestConstants;

@RestController
@RequestMapping(RestConstants.RESOURCE_ITEM)
public class ItemControllerImp implements ItemController {

	@Autowired
	private ItemService itemService;

	@Override
	@ResponseStatus(HttpStatus.OK)
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<List<ItemRest>> getItems() throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				itemService.getItems());
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<ItemRest> createItem(@RequestBody final ItemRest itemRest) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				itemService.createItem(itemRest));
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PutMapping(value = RestConstants.RESOURCE_ID + RestConstants.ITEM, produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<ItemRest> updateItem(@PathVariable Long id, @RequestBody ItemRest itemRest) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK, itemService.updateItem(id, itemRest));
	}
	
	@Override
	@ResponseStatus(HttpStatus.OK)
	@DeleteMapping(value = RestConstants.RESOURCE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<ItemRest> deleteItem(@PathVariable Long id) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK, itemService.deleteItem(id));
	}
}
