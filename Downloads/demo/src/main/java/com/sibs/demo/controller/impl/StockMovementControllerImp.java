package com.sibs.demo.controller.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sibs.demo.controller.StockMovementController;
import com.sibs.demo.exceptions.SibsException;
import com.sibs.demo.json.StockMovementRest;
import com.sibs.demo.responses.SibsResponse;
import com.sibs.demo.service.impl.StockMovementServiceImpl;
import com.sibs.demo.utils.constants.CommonConstants;
import com.sibs.demo.utils.constants.RestConstants;

@RestController
@RequestMapping(RestConstants.RESOURCE_STOCKMOVEMENT)
public class StockMovementControllerImp implements StockMovementController{

	@Autowired
	private StockMovementServiceImpl stockMovementService;

	@Override
	@ResponseStatus(HttpStatus.OK)
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<List<StockMovementRest>> getStockMovement() throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				stockMovementService.getStockMovement());
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<StockMovementRest> createStockMovement(@RequestBody final StockMovementRest stockMovementRest) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK,
				stockMovementService.createStockMovement(stockMovementRest));
	}

	@Override
	@ResponseStatus(HttpStatus.OK)
	@PutMapping(value = RestConstants.RESOURCE_ID + RestConstants.ITEM, produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<StockMovementRest> updateStockMovement(@PathVariable Long id, @RequestBody StockMovementRest stockMovementRest) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK, stockMovementService.updateStockMovement(id, stockMovementRest));
	}
	
	@Override
	@ResponseStatus(HttpStatus.OK)
	@DeleteMapping(value = RestConstants.RESOURCE_ID, produces = MediaType.APPLICATION_JSON_VALUE)
	public SibsResponse<StockMovementRest> deleteStockMovement(@PathVariable Long id) throws SibsException {
		return new SibsResponse<>(CommonConstants.SUCCESS, String.valueOf(HttpStatus.OK), CommonConstants.OK, stockMovementService.deleteStockMovement(id));
	}
}
