package com.sibs.demo.utils.constants;

public class RestConstants {

//	public static final String APPLICATION_NAME = "/sibs";
	public static final String SUCCESS = "Success";

	public static final String RESOURCE_ID = "/{id}";
	public static final String RESOURCE_ITEM = "/sibs/items";
	public static final String RESOURCE_USER = "/sibs/users";
	public static final String RESOURCE_STOCK = "/sibs/stock";
	public static final String RESOURCE_STOCKMOVEMENT = "/sibs/stockmovement";
	public static final String RESOURCE_ORDER = "/sibs/order";
	
	public static final String USER = "/{user}";
	public static final String ORDER = "/{order}";
	public static final String ITEM = "/{item}";
	public static final String STOCK = "/{stock}";
	
	private RestConstants() {
		throw new IllegalStateException("Utility Class");
	}
}
