package com.sibs.demo.json;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.sibs.demo.model.Item;
import com.sibs.demo.model.User;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrderRest implements Serializable {

	private static final long serialVersionUID = -1361696326943581433L;

	private Long id;

	private LocalDateTime creationDate;

	private Item item;

	private Integer quantity;

	private User user;
	
	private Integer quantityCompleted;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(LocalDateTime creationDate) {
		this.creationDate = creationDate;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getQuantityCompleted() {
		return quantityCompleted;
	}

	public void setQuantityCompleted(Integer quantityCompleted) {
		this.quantityCompleted = quantityCompleted;
	}

}
