-- Criar schema sibs
CREATE SCHEMA sibs;

-- Criar tabela para a entidade Item
CREATE TABLE sibs.item (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

-- Criar tabela para a entidade User
CREATE TABLE sibs.user (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL UNIQUE
);

-- Criar tabela para a entidade OrderEntity
CREATE TABLE sibs.order (
    id SERIAL PRIMARY KEY,
    creation_date TIMESTAMP NOT NULL,
    item_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (item_id) REFERENCES sibs.item (id),
    FOREIGN KEY (user_id) REFERENCES sibs.user (id)
);

-- Criar tabela para a entidade StockMovement
CREATE TABLE sibs.stock_movements (
    id SERIAL PRIMARY KEY,
    creation_date TIMESTAMP NOT NULL,
    item_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    FOREIGN KEY (item_id) REFERENCES sibs.item (id)
);

-- Criar tabela para a entidade Stock
CREATE TABLE sibs.stock (
    id SERIAL PRIMARY KEY,
    item_id INTEGER NOT NULL,
    quantity INTEGER NOT NULL,
    FOREIGN KEY (item_id) REFERENCES sibs.item (id)
);

-- Inserir dados na tabela sibs.items
INSERT INTO sibs.item (name)
VALUES ('Mouse'),
       ('Teclado'),
       ('Monitor'),
       ('Headset');

-- Inserir dados na tabela sibs.user
INSERT INTO sibs.user (name, email)
VALUES ('João Silva', 'joao.silva@email.com'),
       ('Maria Santos', 'maria.santos@email.com'),
       ('Lucas Oliveira', 'lucas.oliveira@email.com'),
       ('Ana Souza', 'ana.souza@email.com');
	   
	   
 -- Inserir dados na tabela sibs.stock
INSERT INTO sibs.stock (item_id, quantity)
VALUES (1, 10), -- 10 mouses em estoque
       (2, 5),  -- 5 teclados em estoque
       (3, 8),  -- 8 monitores em estoque
       (4, 6);  -- 6 headsets em estoque
